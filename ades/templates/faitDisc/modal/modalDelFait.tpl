<!-- boîte modale pour la suppression d'un fait disciplinaire -->
<div class="modal fade" id="modalDel" tabindex="-1" role="dialog" aria-labelledby="titleDelete" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="titleDelete">Effacement du fait</h4>
            </div>
            <div class="modal-body" id="formDel">

            </div>
        </div>
    </div>
</div>
