<div id="modalDelPonderation" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalDelPonderationLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="modalDelPonderationLabel">Suppression impossible</h4>
      </div>
      <div class="modal-body">
          <div class="alert alert-danger" role="alert">
              <i class="fa fa-exclamation-circle fa-2x"></i> Cette pondération a été modifiée. Impossible de la supprimer.<br>Veuillez la ramener à la même pondération que "Tous les élèves".
          </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
      </div>
    </div>
  </div>
</div>
