<button type="button"
    class="btn btn-success btn-xs btn-block btn-hire btn-reservation"
    data-heure="{$heure}"
    data-date="{$date}"
    data-idressource="{$idRessource}">
    <i class="fa fa-user"></i>
</button>
